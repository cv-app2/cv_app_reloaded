# ********************************** MODULES **********************************
import pika
import uuid
from flask import Flask, redirect, url_for, render_template, request, Response
from utils.videocamera import VideoCamera

# *********************************** CLASSES *********************************
class RpcClient(object):
    #When the Client starts up, it creates an anonymous exclusive callback queue.
    def __init__(self):
        #We establish a connection
        self.connection = pika.BlockingConnection(pika.URLParameters("amqp://alex:alex@messaging.alex-test-demo.svc:5672"))
        #self.connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        #self.connection = pika.BlockingConnection(pika.URLParameters("amqp://guest:guest@rabbitmq:5672"))
        #We establish a channel
        self.channel = self.connection.channel()
        
        #We declare an exclusive callback_queue for replies.
        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue
        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self,op):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key='rpc_queue',
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=op)
        while self.response is None:
            self.connection.process_data_events()
        return self.response
    
    
# *********************************** MAIN ************************************
#Flask application instanciation :
app = Flask(__name__)


#Definition of the global variables :
global face
#Initialization of the global variables :
face=0

#Definition of the global variables :
global src
global source
global video
#Initialization of the global variables :
src="video/cdanslair.mp4"
source="video/cdanslair.mp4"
video=VideoCamera(src)

def gen():
    # Initialization of a RPC flux 
    frame_rpc = RpcClient()
    global source
    global src
    global video
    global face
    
    if source!=src:
        face=0
        video.__del__()
        video=VideoCamera(src)
        source=src
        
    while True:
        frame=video.get_frame()
        if (type(frame)!=bytes):
            video=VideoCamera(src)
            frame=video.get_frame()
        if face:
            frame = frame_rpc.call(frame)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@app.route('/')
def index():
    return render_template('videosource.html')

@app.route('/video_feed')
def video_feed():
    return Response(gen(),mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/requests',methods=['POST','GET'])
def tasks():
    global face
    global src
    
    if request.method == 'POST':
        
        if request.form.get('webcam') == 'Webcam':
            src=0
        
        if request.form.get('cdanslair') == "C dans l'air":
            src="video/cdanslair.mp4"
        
        if request.form.get('face') == 'Face Detection':
            face=not face
        
        if request.form.get('home') == 'Home':
            return render_template('videosource.html')
    
    elif request.method=='GET':
        return render_template('index.html')
    return render_template('index.html')

if __name__ == '__main__':
    # run() method of Flask class runs the application on the local development server.
    app.run (host = "0.0.0.0", port = 5000)
