# ********************************** MODULES **********************************

import pika
import numpy as np
import cv2
# *********************************** MAIN ************************************


# ##############################  MAIN LOOP  #############################

#Initialization of the connection
#connection = pika.BlockingConnection(pika.URLParameters("amqp://guest:guest@rabbitmq:5672"))
connection = pika.BlockingConnection(pika.URLParameters("amqp://alex:alex@messaging.alex-test-demo.svc:5672"))
#connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()


#Initialization of the object video
#On prend une vidéo au hasard, pour pas avoir l'erreur aucune image à transmettre


#Declaring a rpc queue
channel.queue_declare(queue='rpc_queue')
    
#Callback function that is calle when the server receives a request
#ch: Targeted channel
#method: method for delivery tag
#props
#body: Message body

def on_request(ch, method, props, body):
    #On met en variable globale la source et l'objet video
    #On reçoit le message reçu par le frontend 
    message=body
    #on le transforme en un array
    message = np.asarray(bytearray(message))
    #on le transforme en une image
    message=cv2.imdecode(message, cv2.IMREAD_COLOR)
    #on passe l'image en noir et blanc
    faceCascade=cv2.CascadeClassifier("models/haarcascade_frontalface_default.xml")
    
    grayImg=cv2.cvtColor(message,cv2.COLOR_BGR2GRAY)
    faces=faceCascade.detectMultiScale(grayImg, scaleFactor=1.05, minNeighbors=5)
    #superimposes a rectangle for all the detected images in the frame
    #(x,y) is the top left corner, (x+w, y+h) is the bottom right corner
    #(0,255,0) is colour green and 3 is the thickness of the rectangle edges
    for x, y, w, h in faces:
        message=cv2.rectangle(message, (x,y), (x+w, y+h), (0,255,0), 3)
    #on le transforme en un array
    ret, message = cv2.imencode('.jpg', message)
    response = message.tobytes()
    #Publishing in the channel
    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = \
                                                         props.correlation_id),
                     body=response)
    ch.basic_ack(delivery_tag=method.delivery_tag)

#In case we want to run more than one server process. 
#In order to spread the load equally over multiple server,
#we need to set the prefetch_count setting.
channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='rpc_queue', on_message_callback=on_request)

#Confirmation expected from CPP requests
print(" [x] Awaiting RPC requests")

#Start consuming
channel.start_consuming()


     #Cascade Classifier
    	#https://docs.opencv.org/2.4/modules/objdetect/doc/cascade_classification.html
    	#The Cascade work on the principle that the classifier increasingly 
    	#considers more features for the detection of faces
    	#Starting off with simpler classifiers it increasingly uses them in combination 
    	#for subsequent steps if the prior steps yield positive results
    	#The Haar Cascade Classifier contains pre-trained classication data in XML print("coucou")files
    	#In our case we use the Frontal Face Cascade Classifier
    	#This file must be available for the purpose of classification
    